/*
 * Très Important:  les charactères '@' et '*' sont reservés
 * '@' est l'element neutre, epsilon
 * '*' segnifie le manque de transition entre etats.
 */

#include "automate.h"
#include<iostream>
int menu(){
    char c;
    cout<<"votre choix : ";
    while(cin>>c){
        cin.get();
        switch (c){
        case 'q':
        //quitter    
            return 0;
            break;
        case 'a':
        //afficher le menu
            return 1;
            break;
        case 'c':
         //ajouter char
            return 2;
            break;
        case 'e':
        //ajouter etat
            return 3;
            break;
        case 'i':
        //ajouter init
            return 4;
            break;
        case 'f':
        //ajouter final
            return 5;
            break;
        case 't':
        //ajouter trans
            return 6;
            break;
        case 'v':
        //vider l'automate
            return 7;
            break;
        case '1':
        //supprimer les epsilon transations    
            return 10;
            break;
        case '2':
        //suprimmer les etats innaccessibles    
            return 11;
            break;
        case '3':
        //transformer en afd    
            return 12;
            break;
        case '4':
        //transformer en afd minimal    
            return 13;
            break;
        case '5':
        //afficher l'automate
            return 14;
            break;
        case '6':
        //afficher l'automate
            return 15;
            break;    
            }
    }
}
void afficherMenu(){
    cout<<"q : Quitter"<<endl;
    cout<<"a : afficher le menu"<<endl;
    cout<<"c : ajouter un ou plusieurs charactere a l'alphabet"<<endl;
    cout<<"e : ajouter un etat"<<endl;
    cout<<"i : ajouter un etat initial"<<endl;
    cout<<"f : ajouter un etat final"<<endl;
    cout<<"t : ajouter une transition( @ est la transition epsilon)"<<endl;
    cout<<"v : vider l'automate"<<endl;
    cout<<"1 : suprimer les epsilon transition"<<endl;
    cout<<"2 : suprimer les etats inaccessibles"<<endl;
    cout<<"3 : transformer l'automate a un afd"<<endl;
    cout<<"4 : transformer l'automate a un afd minimal"<<endl;
    cout<<"5 : afficher la liste des transitions de l'automate"<<endl;
    cout<<"6 : afficher l'automate"<<endl;
}
using namespace std;
int main(int argc, char *argv[]){
    Automate test=*(new Automate);
    int eps, afd, inac, m;
    eps=afd=inac=0;
    afficherMenu();
    while(m=menu()){
        switch(m){
            case 1:
                {
                afficherMenu();
                }
                break;
            case 2:
                {
                string a;
                getline(cin, a);
                int i;
                for(i=0; i < a.length(); i++){
                    if(a[i]!='*')
                        test.ajouterChar(a[i]);
                    else
                        cout<<"le charactere* est reserve"<<endl;
                }
                cout<<endl;
                }
                break;
            case 3:
                {
                cout<<"nom de l'etat : ";
                string a1;
                getline(cin, a1);
                test.ajouterEtat(a1);
                }
                cout<<endl;
                break;
            case 4:
                {
                cout<<"nom de l'etat initial : ";
                string a2;
                getline(cin, a2);
                test.ajouterEtatInit(a2);
                }
                cout<<endl;
                break;
            case 5:
                {
                cout<<"nom de l'etat final : ";
                string a3;
                getline(cin, a3);
                test.ajouterEtatFinal(a3);
                }
                cout<<endl;
                break;
            case 6:
                {
                string a4,b4;
                char c;
                cout<<"etat de depart : ";
                getline(cin, a4);
                cout<<"transition par : ";
                c=cin.get();
                while(cin.get()!='\n');
                cout<<"etat d'arrivee : ";
                getline(cin, b4);
                test.ajouterTransition(a4, c, b4);
                }
                cout<<endl;
                break;
             case 7:
                {
                inac=eps=afd=0;
                Automate * x_x=&test;
                test = *(new Automate);
                x_x->~Automate();
                }
                cout<<endl;
                break;
             case 10:
                {
                eps=1;
                test=test.supTransEpsilon();
                }
                cout<<endl;
                break;
             case 11:
                {
                    inac=1;
                    test=test.supEtatsInaccess();
                }
                cout<<endl;
                break;
             case 12:
                {
                    afd=1;
                    if(!eps){
                        eps=1;
                        test=test.supTransEpsilon();
                    }
                    if(!inac){
                        inac=1;
                        test=test.supEtatsInaccess();
                    }
                    test=test.transEnAFD();
                }
                cout<<endl;
                break;
             case 13:
                {
                    if(!eps){
                        eps=1;
                        test=test.supTransEpsilon();
                    }
                    if(!inac){
                        inac=1;
                        test=test.supEtatsInaccess();
                    }
                    if(!afd){
                        afd=1;
                        test=test.transEnAFD();
                    }
                    test=test.transEnAFDMin();
                }
                cout<<endl;
                break;
             case 14:
                test.listerTransitions();
                cout<<endl;
                break;
             case 15:
                test.afficherAutomate();
                cout<<endl;
                break;
        }
    }
    return(0);
} 
