#include<iostream>
#include <list>
#include "automate.h"
#include<fstream>
using namespace std;
Automate::Automate(){
    nombre_etats=0;
    alphabet+='@';
}
int inlist(list<string>::iterator a,list<string>::iterator b, string c){
    while(a!=b){
        if(c.compare(*a)==0){
            return 1;
        }
        a++;
    }
    return 0;
}
int inlist(list<int>::iterator a,list<int>::iterator b, int c){
    while(a!=b){
        if(c==(*a)){
            return 1;
        }
        a++;
    }
    return 0;
}
int indexof(list<string>::iterator a,list<string>::iterator b, string c){
    int count = 0 ;
    while(a!=b){
        if(c.compare(*a)==0){
            return count;
        }
        a++,count++;
    }
    return -1;
}
string remEps(string c){
    int a;
    while ((a=c.find("@"))!=c.npos){
        c.erase(a,1);
    }
    return c;
}
int complists(list<int > a, list<int> b){
    if(a.size()!=b.size())
        return 0;
    list<int>::iterator c,d;
    for(c=a.begin(), d=b.begin();c!=a.end(); c++,d++){
        if((*c)!=(*d))
            return 0;
    }
    return 1;
}
int compll(list<list<int> > a, list<list<int> > b){
    if(a.size()!=b.size()) return 0;
    list<list<int> >::iterator c,d;
    for(c=a.begin(), d=b.begin();c!=a.end(); c++,d++){
        if(!complists((*c),(*d)))
            return 0;
    }
    return 1;
}
string Automate::getEtat(int n){
    if (n<nombre_etats){
        list<string>::iterator a=etats.begin();
        for (int i=0;i<n;i++){
            a++;
        }
        return (*a);
    }
    cerr<<"invalid index";
    return "";
}

void Automate::ajouterEtat(string a){
    if(!inlist(etats.begin(), etats.end(), a)){
        etats.push_back(a);
        nombre_etats++;
        list<list<string> >::iterator li=transitions.begin();
        while(li!=transitions.end()){
            (*li).push_back("*");
            li++;
        }
        list<string> nt;
        for(int i=0;i<nombre_etats;i++)
            nt.push_back("*");
        transitions.push_back(nt);
    }
    else
        cerr<<" etat existant\n";
}
void Automate::ajouterChar(){
    char a;
    cin>>a;
    if(alphabet.find(a, 0)==alphabet.npos)
        alphabet=alphabet+a;
    else
        cerr<<a<<" est deja dans l'alphabet\n";
};
void Automate::ajouterChar(char a){
    if(alphabet.find(a)==alphabet.npos)
        alphabet=alphabet+a;
    else
        cerr<<a<<" est deja dans l'alphabet\n";
};
void Automate::ajouterEtatInit(string a){
    if(!inlist(etats.begin(), etats.end(), a))
        ajouterEtat(a);
    initial=a;
}
void Automate::ajouterEtatFinal(string a){
    if(!inlist(etats.begin(), etats.end(), a)){
        ajouterEtat(a);
        finaux.push_back(a);
    }
    else if(!inlist(finaux.begin(),finaux.end(), a))
        finaux.push_back(a);
}
string Automate::getTransition(int ligne, int colonne){
    list<list<string> >::iterator i1=transitions.begin();
    for(int i=0;i<ligne; i++)i1++;
    list<string>::iterator i2= (*i1).begin();
    for(int i=0;i<colonne; i++)i2++;
    return (*i2);
}
void Automate::setTransition(int ligne, int colonne, string a){
    list<list<string> >::iterator i1=transitions.begin();
    for(int i=0;i<ligne; i++)i1++;
    list<string>::iterator i2= (*i1).begin();
    for(int i=0;i<colonne; i++)i2++;
    (*i2)=a;
}
void Automate::ajouterTransition(string a, char cou, string b){
    if((inlist(etats.begin(), etats.end(), a)) && (inlist(etats.begin(), etats.end(), b)) &&(alphabet.find(cou,0)!=alphabet.npos)){
        if(getTransition(indexof(etats.begin(), etats.end(), a), indexof(etats.begin(), etats.end(), b )).compare("*")!=0)
            setTransition(indexof(etats.begin(), etats.end(), a), indexof(etats.begin(), etats.end(), b), getTransition(indexof(etats.begin(), etats.end(), a), indexof(etats.begin(), etats.end(), b ))+string(1,cou));
        else
            setTransition(indexof(etats.begin(), etats.end(), a), indexof(etats.begin(), etats.end(), b), string(1,cou));
    }
    else
        cerr<<"input errone, etats inexistants ou charactere n'appartient pas a l'alphabet\n";
}
void Automate::listerTransitions(){
    int i,j;
    cout<<endl;
    if(nombre_etats!=0){
    for(i=0;i<nombre_etats;i++){
        if (i==0){
            int k;
            cout<<"\t";
            for (k=0;k<nombre_etats;k++)
                cout<<getEtat(k)<<"\t";
        }
        cout<<endl;
        for(j=0;j<nombre_etats;j++){
            if(j==0)
                cout<<getEtat(i)<<"\t";
            cout<<getTransition(i,j)<<"\t";
        }
        cout<<endl;
    }
    cout<<endl;}
    else
        cerr<<"automate vide\n";
}

Automate Automate::supEtatsInaccess(){
    Automate ret;
    ret.alphabet=alphabet;
    list <string> a;
    a.push_front(initial);
    ret.ajouterEtatInit(initial);
    int i,j;
    while(!a.empty()){
        for( i=0;i<nombre_etats;i++){
            if(getTransition(indexof(etats.begin(), etats.end(), (a.front())), i).compare("*")!=0){
                if (!inlist(ret.etats.begin(), ret.etats.end(), getEtat(i)))
                    a.push_back(getEtat(i));
            }
        }
        if (!inlist(ret.etats.begin(), ret.etats.end(), a.front() ) )
        	ret.ajouterEtat(a.front());
        if(inlist(finaux.begin(), finaux.end(), (a.front())))
            ret.ajouterEtatFinal(a.front());
        a.pop_front();
    }
    for(i=0;i<ret.nombre_etats;i++){
        for(j=0;j<ret.nombre_etats;j++){
            ret.setTransition(i,j, 
                    getTransition(indexof(etats.begin(),etats.end(),ret.getEtat(i)), 
                    indexof(etats.begin(),etats.end(),ret.getEtat(j))));
        }
    }
    return ret;
}
Automate Automate::supTransEpsilon(){
    Automate ret;
    int i,j,k, flag=true;
    ret.initial=initial;
    ret.alphabet=alphabet;
    for(i=0;i<nombre_etats;i++){
        ret.ajouterEtat(getEtat(i));
        if(inlist(finaux.begin(), finaux.end(), getEtat(i)))
            ret.ajouterEtatFinal(getEtat(i));
    }
    for(i=0;i<nombre_etats;i++){
        for(j=0;j<nombre_etats;j++)
            ret.setTransition(i,j , getTransition(i,j));
    }
    k=0;
    while((k<ret.nombre_etats)&&(flag)){
        flag=false;
        for(i=0;i<ret.nombre_etats;i++){
            for(j=0;j<ret.nombre_etats;j++){
                if(ret.getTransition(i,j).find("@")!=ret.getTransition(i,j).npos){
                    flag=true;
                    if (inlist(ret.finaux.begin(), ret.finaux.end(), ret.getEtat(j)))
                        ret.ajouterEtatFinal(ret.getEtat(i));
                    int l;
                    for(l=0;l<ret.nombre_etats;l++){
                        if(ret.getTransition(j,l).compare("*")!=0){
                            int m=0;
                            while(m<ret.getTransition(j,l).size()){
                                char a=ret.getTransition(j,l)[m++];
                                if(ret.getTransition(i,l).find(a)==ret.getTransition(i,l).npos)
                                    ret.ajouterTransition(getEtat(i), a, getEtat(l));
                            }
                        }
                    }
                    ret.setTransition(i,j,remEps(ret.getTransition(i,j)));
                    if(ret.getTransition(i,j).empty())
                        ret.setTransition(i,j,"*");
                }
            }
        }
        k++;
    }
    return ret;
}

Automate Automate::transEnAFD(){
    Automate ret;
    ret.alphabet=alphabet;
    list<list<int> >nouvaux_etats;
    list<int> placeholder;
    placeholder.push_back(indexof(etats.begin(), etats.end(),initial));
    int nom_etat=65;
    string an="";
    ret.ajouterEtatInit(an+(char)(nom_etat++));
    nouvaux_etats.push_back(placeholder);
    list<list<int > >::iterator it=nouvaux_etats.begin();
    int count=0;
    while(it!=nouvaux_etats.end()){
        int i;
        for(i=1;i<ret.alphabet.length(); i++){
            list<int> aph;
            list<int>::iterator ai=(*it).begin();
            int final = false;
            for(;ai!=(*it).end(); ai++){
                int j;
                for(j=0;j<nombre_etats; j++){
                    if(getTransition((*ai), j).find(alphabet[i])!=getTransition((*ai), j).npos){
                        aph.push_back(j);
                        if(inlist(finaux.begin(), finaux.end(), getEtat(j)))
                            final=true;
                    }
                }
            }
            aph.sort();
            aph.unique();
            list<list<int> >::iterator yali=nouvaux_etats.begin();
            int resc=true, pos=0;
            if(!aph.empty()){
            for(;yali!=nouvaux_etats.end();yali++){
                if(complists(aph,(*yali))){
                    resc=false;
                    break;
                }
                pos++;
            }
            if(resc){
                nouvaux_etats.push_back(aph);
                final?ret.ajouterEtatFinal(an+(char)(nom_etat++)):ret.ajouterEtat(an+(char)(nom_etat++));
                ret.ajouterTransition(ret.getEtat(count), alphabet[i], ret.getEtat(ret.nombre_etats-1));
            }
            else
                ret.ajouterTransition(ret.getEtat(count), alphabet[i], ret.getEtat(pos));
        }
        }
        it++;
        count++;
    }
    return ret;
}
string Automate::getTransDet(string a, char b){
    int j;
    string ret;
    for(j=0;j<nombre_etats; j++){
        if(getTransition(indexof(etats.begin(), etats.end(), a), j).find(b)!=getTransition(indexof(etats.begin(), etats.end(), a), j).npos){
            ret=getEtat(j);
            break;
        }
    }
    return ret;
}
void cp(list<list<int> > a, list<list<int> > *b){
    b->clear();
    list<list<int> >::iterator c=a.begin();
    for(; c!=a.end();c++){
        list<int> x;
        list<int>::iterator y=(*c).begin();
        while(y!=(*c).end()){
            x.push_back((*y));
            y++;
        }
        b->push_back(x);
    }
}
Automate Automate::transEnAFDMin(){
    Automate ret;
    ret.alphabet=alphabet;
    list<list<int> > nouvaux_etats, nes;
    list<int> ini, fin;
    int i,j;
    for(i=0;i<nombre_etats;i++)
        inlist(finaux.begin(), finaux.end(), getEtat(i))?fin.push_back(i):ini.push_back(i);
    nouvaux_etats.push_back(ini);
    nes.push_back(ini);
    nouvaux_etats.push_back(fin);
    nes.push_back(fin);
    int *tt=new int[nombre_etats*(alphabet.size()-1)];
    do{
        cp(nes, &nouvaux_etats);
        nes.clear();
        for (i=0;i<nombre_etats; i++) {
            for(j=1;j<alphabet.size(); j++){
                if(getTransDet(getEtat(i), alphabet[j]).empty())
                    tt[(j-1)*nombre_etats+i]=-1;
                else{
                    int l,in = indexof(etats.begin(), etats.end(), getTransDet(getEtat(i), alphabet[j]));
                    list<list<int> >::iterator k=nouvaux_etats.begin();
                    for(l=0;k!=nouvaux_etats.end();k++){
                        if(inlist((*k).begin(), (*k).end(), in))
                            break;
                        l++;
                    }
                    tt[(j-1)*nombre_etats+i]=l;
                }
            }
        }
        for(i=0;i<nouvaux_etats.size(); i++){
            list<int> x;
            nes.push_back(x);
        }
        for(i=0;i<nombre_etats;i++){
            int l;
            list<list<int> >::iterator k=nouvaux_etats.begin(), m=nes.begin();
            for(l=0;k!=nouvaux_etats.end();k++){
                        if(inlist((*k).begin(), (*k).end(), i))
                            break;
                        l++, m++;
                    }
            if((*m).empty())
                (*m).push_back(i);
            else{
                list<int> a, b;
                for(j=1; j<alphabet.size()-1;j++)
                    a.push_back(tt[(j-1)*nombre_etats+i]), b.push_back(tt[ (j-1)*nombre_etats +(*m).front()]);
                if(complists(a,b))
                    (*m).push_back(i);
                else{
                    int c, ne=false, ev;
                    k=nouvaux_etats.begin();
                    for(c=0; c<=l; c++, k++);
                    list <int>::iterator d=(*k).begin();
                    while((*d)<i){
                        int e;
                        b.clear();
                        for(e=1; e<alphabet.size()-1;e++)
                            b.push_back(tt[(e-1)*nombre_etats+(*d)]);
                        if(complists(a,b)){
                            ne=true;
                            ev=(*d);
                            break;
                        }
                        d++;
                    }
                    if(ne){
                        list<list<int> >::iterator ni=nes.begin();
                        for(;ni!=nes.end();ni++){
                            if(inlist((*ni).begin(),(*ni).end(), ev)){
                                (*ni).push_back(i);
                                break;
                            }
                        }
                    }
                    else{
                        list<int> x;
                        x.push_back(i);
                        nes.push_back(x);
                    }
                }
            }
        }
    }
    while(!compll(nouvaux_etats, nes));
    list<list<int> >::iterator a=nes.begin();
    char ac='A';
    string ph="";
    for(;a!=nes.end();a++){
        if(inlist((*a).begin(), (*a).end(), indexof(etats.begin(), etats.end(),initial)))
            ret.ajouterEtatInit(ph+ac);
        else{
            int final=false;
            list<int>::iterator b=(*a).begin();
            for(;b!=(*a).end(); b++){
                if(inlist(finaux.begin(), finaux.end(), getEtat((*b)))){
                    final=true;
                    break;
                }
            }
            final?ret.ajouterEtatFinal(ph+ac):ret.ajouterEtat(ph+ac);
        }
        ac++;
    }
    a=nes.begin();
    for(i=0;a!=nes.end();a++,i++){
        for(j=0;(j<alphabet.size()-1);j++){
            if(tt[j*nombre_etats+(*a).front()]!=-1)
                ret.ajouterTransition(ret.getEtat(i), alphabet[j+1], ret.getEtat(tt[j*nombre_etats+(*a).front()]));
        }
    }
    return ret;
}
void Automate::afficherAutomate(){
	cout<<endl;
    cout<<"Automate={\n";
    cout<<"etats= { ";
    int i,j;
    for(i=0;i<nombre_etats;i++){
        cout<<getEtat(i);
        if(i!=(nombre_etats-1))
            cout<<", ";
    }
    cout<<" },\n";
    cout<<"alphabet = {";
    for(i=1;i<alphabet.length();i++){
        cout<<alphabet[i];
        if(i!=(alphabet.length()-1))
        cout<<",";
    }
    cout<<" },\n";
    cout<<"transitions = {\n";
    for(i=0;i<nombre_etats;i++){
        for(j=0;j<nombre_etats;j++){
            if(getTransition(i,j).compare("*")!=0){
                cout<<"("<<getEtat(i)<<" , "<<getTransition(i,j)<<" , "<<getEtat(j)<<")\n";
            }
        }
    }
    cout<<"},\n";
    cout<< "initial = {"<<initial<<" },\n";
    cout<<"finaux = { ";
    list<string>::iterator it=finaux.begin();
    while(it!=finaux.end()){
        cout<<(*it);
        it++;
        if( it!=finaux.end() )
            cout<<", ";
    }
    cout<<"}\n}";
}
