#ifndef AUTOMATE
using namespace std;
#include <string>
#include<list>
class Automate{
    public:
        string alphabet;
        list <string> etats;
        string  initial;
        list<string> finaux;
        list<list<string> > transitions;
        int nombre_etats;
    public:
        Automate();
        Automate supEtatsInaccess();
        Automate transEnAFD();
        Automate transEnAFDMin();
        Automate supTransEpsilon();
        void ajouterChar(char a);
        void ajouterChar();
        void ajouterEtat(string a);
        void ajouterEtatInit(string a);
        void ajouterEtatFinal(string a);
        void ajouterTransition(string a, char cout, string b);
        string getTransition(int ligne, int colonne);
        void setTransition(int ligne, int colonne, string a);
        string getEtat(int n);
        void listerTransitions();
        string getTransDet(string a, char b);
        void afficherAutomate();
};
int inlist(list<string>::iterator a,list<string>::iterator v, string b);
int inlist(list<int>::iterator a,list<int>::iterator b, int c);
int indexof(list<string>::iterator a,list<string>::iterator v, string b);
string remEps(string c);
int complists(list<int > a, list<int> b);
int compll(list<list<int> > a, list<list<int> > b);
void cp(list<list<int> > , list<list<int> >*);


#define AUTOMATE
#endif
